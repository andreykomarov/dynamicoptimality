
def between(x1, x2, x):
    return min(x1, x2) <= x <= max(x1, x2)

def inside(p1, p2, p):
    return between(p1[0], p2[0], p[0]) and between(p1[1], p2[1], p[1])

def ok(p1, p2, px):
    if p1[0] == p2[0] or p1[1] == p2[1]:
        return True
    return any(inside(p1, p2, p) for p in px if p1 != p and p2 != p)

def bad(px):
    return any(not ok(p1, p2, px) for p1 in px for p2 in px)

def check(init, add):
    assert not bad(init + add)
    for i in range(len(add)):
        assert bad(init + add[:i] + add[i+1:])

init, add = eval(input()) # МММАКСИМУМ БЕЗОПАСНОСТИ

check(init, add)

print(r"""verbatimtex
\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\begin{document}
etex""")

print("beginfig(1)")
print("u = 1cm;")

def sq(x, y, c):
    return "fill ({}u, {}u)--({}u, {}u)--({}u, {}u)--({}u, {}u)--cycle withcolor {};".format(x, y, x+1, y, x+1, y+1, x, y+1, c)

for x, y in init:
    print(sq(x, y, "blue"))

for x, y in add:
    print(sq(x, y, "red"))

print("endfig;")
print("end.")

print(init)
print(add)
