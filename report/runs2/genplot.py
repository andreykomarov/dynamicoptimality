import matplotlib
matplotlib.rc('text', usetex = True)
import pylab
import numpy as np
from sys import argv
 
assert len(argv) == 3
fname = argv[1]

x, y1, y2, y3 = [], [], [], []
 
for line in open(argv[1]).readlines():
    a = [int(_) for _ in line.split('&')[:4]]
    x.append(a[0])
    y1.append(a[1])
    y2.append(a[2])
    y3.append(a[3])
   
pylab.step(x, y1, "g-", x, y2, "b-", x, y3, "r-")
 
pylab.legend(("Splay", "Greedy", "Genetic algorithm"), loc='upper left')
pylab.xlabel('$n$')
pylab.ylabel('ASS size')

pylab.savefig(argv[2], dpi=300) 
#pylab.show()
