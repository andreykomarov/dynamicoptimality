import matplotlib
matplotlib.rc('text', usetex = True)
import matplotlib.pyplot as plt
import os
from sys import argv
from collections import Counter

assert len(argv) == 3
fname = argv[1]

d = Counter()

for line in open(fname):
    s = line.split()
    n, val = int(s[0]), float(s[1])
    d[n] = max(d[n], val)

ns, ys = list(zip(*d.items()))

pairs = sorted(zip(ns, ys))
ns = map(lambda t: t[0], pairs)
ys = map(lambda t: t[1], pairs)

plt.plot(ns, ys, "bo")
plt.xlabel("$n$")
plt.ylabel("$\mathrm{Splay}(n) / \mathrm{Greedy}(n)$")
plt.savefig(argv[2], dpi=300)
#plt.show()
