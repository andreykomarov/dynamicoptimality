import java.util.ArrayList;

public class DeepestInSplayTree {
    static public ArrayList<Integer> queries(int n) {
        boolean[] used = new boolean[n];
        SplayTree t = new SplayTree(n);
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int[] depths = t.depths();
            Integer best = null;
            for (int j = 0; j < n; j++) {
                if (!used[j] && (best == null || depths[j] > depths[best])) {
                    best = j;
                }
            }
//            System.err.println(Arrays.toString(depths) + " " + depths[best] + " " + best);
            t.find(best);
            res.add(best);
            used[best] = true;
        }
        return res;
    }
}
