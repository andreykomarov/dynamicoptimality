import matplotlib
import matplotlib.pyplot as plt
import os

ns, ys = [], []

for root, dirs, files in os.walk('logs'):
    for dir in dirs:
        n = int(dir)
        best_fitness = 0
        for _, _,  logs in os.walk(os.path.join(root, dir)):
            for log in logs:
                for line in open(os.path.join(root, dir, log), "r"):
                    val = float(line.split()[3])
                    best_fitness = max(best_fitness, val)
        ns.append(n)
        ys.append(best_fitness)

pairs = sorted(zip(ns, ys))
ns = map(lambda t: t[0], pairs)
ys = map(lambda t: t[1], pairs)

plt.plot(ns, ys, "yo-")
plt.savefig('plot.png')
plt.show()
