import java.util.List;

public class SplayTree {
    private static class Node {

        int value;
        Node left, right, parent;

        private Node(int value, Node left, Node right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        private void rotateRight() {
            Node b = right;
            assert parent.left == this;
            right = parent;
            right.left = b;
            if (b != null)
                b.parent = right;
            parent = right.parent;
            right.parent = this;
            if (parent != null && parent.left == right)
                parent.left = this;
            else if (parent != null && parent.right == right)
                parent.right = this;
        }

        private void rotateLeft() {
            Node b = left;
            assert parent.right == this;
            left = parent;
            left.right = b;
            if (b != null)
                b.parent = left;
            parent = left.parent;
            left.parent = this;
            if (parent != null && parent.left == left)
                parent.left = this;
            else if (parent != null && parent.right == left)
                parent.right = this;
        }

        public void rotate() {
            assert parent != null;
            if (parent.left == this) {
                rotateRight();
            } else {
                rotateLeft();
            }
        }

    }

    private Node root;

    static Node balanced(int left, int right) {
        if (left == right) {
            return null;
        } else if (left + 1 == right) {
            return new Node(left, null, null);
        } else {
            int mid = (left + right) / 2;
            Node fst = balanced(left, mid);
            Node snd = balanced(mid + 1, right);
            Node res = new Node(mid, fst, snd);
            if (fst != null)
                fst.parent = res;
            if (snd != null)
                snd.parent = res;
            return res;
        }
    }

    public SplayTree(int n) {
//        for (int i = 0; i < n; i++) {
//            root = new Node(i, root, null);
//            if (root.left != null)
//                root.left.parent = root;
//        }
        root = balanced(0, n);
    }

    public int find(int x) {
        Node me = root;
        int count = 0;
        while (me != null) {
            count++;
            int val = me.value;
            if (val == x) {
                break;
            } else if (x < val) {
                me = me.left;
            } else {
                me = me.right;
            }
        }
        if (me == null) {
            throw new AssertionError("No such key in splay tree: " + x);
        }
        splay(me);
        return count;
    }

    void splay(Node t) {
        Node parent = t.parent;
        if (parent == null) {
            root = t;
            return;
        }
        Node gparent = parent.parent;
        if (gparent == null) {
            t.rotate();
            root = t;
            return;
        }
        if ((parent == gparent.left) == (t == parent.left)) {
            parent.rotate();
        } else {
            t.rotate();
        }
        t.rotate();
        splay(t);
    }

    private void traverse(Node t, int depth, int[] depths) {
        depths[t.value] = depth;
        if (t.left != null) {
            traverse(t.left, depth + 1, depths);
        }
        if (t.right != null) {
            traverse(t.right, depth + 1, depths);
        }
    }

    int[] depths() {
        Node max = root;
        while (max.right != null) {
            max = max.right;
        }
        int[] res = new int[max.value + 1];
        traverse(root, 0, res);
        return res;
    }

    static int process(List<Integer> queries) {
        int n = -1;
        for (int i : queries) {
            n = Math.max(n, i);
        }
        SplayTree t = new SplayTree(n + 1);
        int res = 0;
        for (int i : queries) {
            res += t.find(i);
        }
        return res;
    }
}
