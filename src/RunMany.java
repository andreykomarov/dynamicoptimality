import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.operators.ListOrderCrossover;
import org.uncommons.watchmaker.framework.operators.ListOrderMutation;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.Stagnation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class RunMany {

    static final int MINN = 10;
    static final int MAXN = 100;
    static final int ITERATIONS = 7;
    static final int STEPS_BEFORE_STAGNATION = 100;

    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter log = new PrintWriter("log-" + System.currentTimeMillis() + ".log");
        for (int n = MINN; n <= MAXN; n++) {
            List<Integer> bestPerm = null;
            double best = -1;
            for (int it = 0; it < ITERATIONS; it++) {
                @SuppressWarnings("unchecked")
                final List<Integer>[] bestPermForIteration = new List[]{null};
                final double[] bestForIteration = {-1};
                List<EvolutionaryOperator<List<Integer>>> operators = new ArrayList<>();
                operators.add(new ListOrderMutation<Integer>(5, 5));
                operators.add(new ListOrderCrossover<Integer>());
                EvolutionaryOperator<List<Integer>> pipeline = new EvolutionPipeline<>(operators);
                GenerationalEvolutionEngine<List<Integer>> engine = new GenerationalEvolutionEngine<>(
                        new Genetics.IntPermutationFactory(n)
                        , pipeline
                        , new Genetics.GreedyVsSplayEvaluator()
                        , new RouletteWheelSelection()
                        , new MersenneTwisterRNG()
                );
                engine.setSingleThreaded(false);
                engine.addEvolutionObserver(new EvolutionObserver<List<Integer>>() {
                    @Override
                    public void populationUpdate(PopulationData<? extends List<Integer>> data) {
                        double fitness = data.getBestCandidateFitness();
                        List<Integer> bestCandidate = data.getBestCandidate();
                        System.out.println("Generation #" + data.getGenerationNumber() + " " +
                                "Best: " + fitness + " " +
                                bestCandidate.toString());
                        if (fitness > bestForIteration[0]) {
                            bestForIteration[0] = fitness;
                            bestPermForIteration[0] = new ArrayList<>(bestCandidate);
                        }
                    }
                });
                TerminationCondition finish = new Stagnation(STEPS_BEFORE_STAGNATION * n, true);
                engine.evolve(300, 5, finish);
                log.println("IT " + n + " " + it + " " + bestForIteration[0] + " " + bestPermForIteration[0]);
                log.flush();
                if (best < bestForIteration[0]) {
                    best = bestForIteration[0];
                    bestPerm = new ArrayList<>(bestPermForIteration[0]);
                }
            }
            log.println("TOTAL " + n + " " + best + " " + bestPerm);
            log.flush();
        }
    }
}
