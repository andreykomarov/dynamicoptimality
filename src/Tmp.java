import java.util.ArrayList;

public class Tmp {
    public static void main(String[] args) {
        for (int n = 10; n < 10000; n++) {
            ArrayList<Integer> q = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                q.add(i);
            }
//            Collections.shuffle(q);
//            ArrayList<Integer> q = DeepestInSplayTree.queries(n);
            int splay = SplayTree.process(q);
            int greedy = Greedy.process(q);
            double ratio = splay * 1. / greedy;
            System.err.println(n + " " + ratio);
        }
    }
}
