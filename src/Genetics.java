import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Genetics {
    static class GreedyVsSplayEvaluator implements FitnessEvaluator<List<Integer>> {
        @Override
        public double getFitness(List<Integer> candidate, List<? extends List<Integer>> population) {
            return SplayTree.process(candidate) * 1. / Greedy.process(candidate);
        }

        @Override
        public boolean isNatural() {
            return true;
        }
    }

//    static class IntArrayPointMutate implements EvolutionaryOperator<int[]> {
//        int range;
//
//        IntArrayPointMutate(int range) {
//            this.range = range;
//        }
//
//        @Override
//        public List<int[]> apply(List<int[]> selectedCandidates, Random rng) {
//            ArrayList<int[]> res = new ArrayList<>();
//            for (int[] xs : selectedCandidates) {
//                int[] xs2 = xs.clone();
//                xs2[rng.nextInt(xs2.length)] = rng.nextInt(range);
//                res.add(xs2);
//            }
//            return res;
//        }
//    }

//    static class IntArrayMutate implements EvolutionaryOperator<int[]> {
//        double prob;
//        int range;
//
//        IntArrayMutate(double prob, int range) {
//            this.prob = prob;
//            this.range = range;
//        }
//
//        @Override
//        public List<int[]> apply(List<int[]> selectedCandidates, Random rng) {
//            ArrayList<int[]> res = new ArrayList<>();
//            for (int[] xs : selectedCandidates) {
//                int[] xs2 = xs.clone();
//                for (int i = 0; i < xs2.length; i++) {
//                    if (rng.nextDouble() < prob) {
//                        xs2[i] = rng.nextInt(range);
//                    }
//                }
//                res.add(xs2);
//            }
//            return res;
//        }
//    }

    static class IntPermutationFactory extends AbstractCandidateFactory<List<Integer>> {

        final int n;

        IntPermutationFactory(int n) {
            this.n = n;
        }

        @Override
        public List<Integer> generateRandomCandidate(Random rng) {
            ArrayList<Integer> perm = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                perm.add(i);
            }
            Collections.shuffle(perm, rng);
            return perm;
        }
    }

    static class IntArrayFactory extends AbstractCandidateFactory<int[]> {
        int length;
        int range;

        IntArrayFactory(int length, int range) {
            this.length = length;
            this.range = range;
        }

        @Override
        public int[] generateRandomCandidate(Random rng) {
            int[] res = new int[length];
            for (int i = 0; i < length; i++) {
                res[i] = rng.nextInt(range);
            }
            return res;
        }
    }
}
