import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.operators.ListOrderCrossover;
import org.uncommons.watchmaker.framework.operators.ListOrderMutation;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.UserAbort;

import java.util.ArrayList;
import java.util.List;

public class Main {

    static final int N = 35;
//    static final int M = 0;

    public static void main(String[] args) {
        List<EvolutionaryOperator<List<Integer>>> operators = new ArrayList<>();
//        operators.add(new Genetics.IntArrayPointMutate(N));
        operators.add(new ListOrderMutation<Integer>(1, 1));
        operators.add(new ListOrderMutation<Integer>(5, 5));
//        operators.add(new Genetics.IntArrayMutate(0.15, M));
        operators.add(new ListOrderCrossover<Integer>());
//        operators.add(new IntArrayCrossover(2));
        EvolutionaryOperator<List<Integer>> pipeline = new EvolutionPipeline<>(operators);
        GenerationalEvolutionEngine<List<Integer>> engine = new GenerationalEvolutionEngine<List<Integer>>(
//                new Genetics.IntArrayFactory(M, N)
                new Genetics.IntPermutationFactory(N)
                , pipeline
                , new Genetics.GreedyVsSplayEvaluator()
                , new RouletteWheelSelection()
                , new MersenneTwisterRNG()
        );
        engine.setSingleThreaded(false);
        engine.addEvolutionObserver(new EvolutionObserver<List<Integer>>() {
            @Override
            public void populationUpdate(PopulationData<? extends List<Integer>> data) {
                System.out.println("Generation #" + data.getGenerationNumber() + " " +
                                   "Best: " + data.getBestCandidateFitness() + " " +
                                   data.getBestCandidate().toString());
            }
        });
        TerminationCondition finish = new UserAbort();
//        TerminationCondition finish = new Stagnation(1000, true);

        engine.evolve(100, 3, finish);
    }
}
