import java.util.HashMap;
import java.util.TreeSet;

public class ASS {

    private HashMap<Integer, TreeSet<Integer>> xs;
    private HashMap<Integer, TreeSet<Integer>> ys;

    public ASS() {
        xs = new HashMap<>();
        ys = new HashMap<>();
    }

    private boolean isSatisfied(int x1, int x2, TreeSet<Integer> ts) {
        if (x1 > x2) {
            int tmp = x1;
            x1 = x2;
            x2 = tmp;
        }
        Integer next = ts.ceiling(x1);
        if (next == null)
            return false;
        return next <= x2;
    }

    private boolean isSatisfiedX(int x, int y1, int y2) {
        if (!xs.containsKey(x)) {
            return false;
        }
        return isSatisfied(y1, y2, xs.get(x));
    }

    private boolean isSatisfiedY(int y, int x1, int x2) {
        if (!ys.containsKey(y)) {
            return false;
        }
        return isSatisfied(x1, x2, ys.get(y));
    }

    private void add(HashMap<Integer, TreeSet<Integer>> xs, int x, int y) {
        if (!xs.containsKey(x)) {
            xs.put(x, new TreeSet<Integer>());
        }
        xs.get(x).add(y);
    }

    public void add(int x, int y) {
        add(xs, x, y);
        add(ys, y, x);
    }

    public boolean isSatisfied(int x1, int y1, int x2, int y2) {
        if (x1 == x2 || y1 == y2)
            return true;
        if (x1 > x2) {
            int tmp = x1;
            x1 = x2;
            x2 = tmp;
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
        if (isSatisfiedY(y1, x1 + 1, x2))
            return true;
        if (isSatisfiedY(y2, x1, x2 - 1))
            return true;
        if (y1 > y2) {
            int tmp = y1;
            y1 = y2;
            y2 = tmp;
            tmp = x1;
            x1 = x2;
            x2 = tmp;
        }
        if (isSatisfiedX(x1, y1 + 1, y2))
            return true;
        if (isSatisfiedX(x2, y1, y2 - 1))
            return true;
        return false;
    }

}
