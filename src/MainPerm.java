import java.util.ArrayList;
import java.util.Arrays;

public class MainPerm {

    static final int N = 40;
    static final int M = 40;

    static boolean nextPermutation(int[] a) {
        int n = a.length;
        int last;
        for (last = n - 1; last >= 1 && a[last] < a[last - 1]; last--)
            ;
        if (last == 0)
            return false;
        int next = last;
        for (int i = last; i < n; i++) {
            if (a[i] > a[last - 1] && a[i] < a[next])
                next = i;
        }
        int tmp = a[next];
        a[next] = a[last - 1];
        a[last - 1] = tmp;
        Arrays.sort(a, last, n);
        return true;
    }

    static boolean next(int[] a) {
        int n = a.length;
        int last;
        for (last = n - 1; last >= 0 && a[last] == n; last--)
            ;
        if (last == -1)
            return false;
        for (int i = last + 1; i < n; i++) {
            a[i] = 0;
        }
        a[last]++;
        return true;
    }

    public static void main(String[] args) {

        int MAXN = 20;
        int[][] bestP = new int[MAXN + 1][];
        double[] best = new double[MAXN + 1];
        for (int N = 1; N < 12; N++) {
            int[] p = new int[N];
            int[] pp = new int[2 * N];
            for (int i = 0; i < p.length; i++) {
                p[i] = i;
            }
            best[N] = 0;
            int i = 0;
            do {
                for (int k = 0; k < N; k++) {
                    pp[k] = pp[k + N] = p[k];
                }
                ArrayList<Integer> ppa = new ArrayList<>();
                for (int ii : pp) {
                    ppa.add(ii);
                }
                int sp = SplayTree.process(ppa);
                int gr = Greedy.process(ppa);
                double f = sp * 1. / gr;
                if (f > best[N]) {
                    System.err.println(N + " : " + f + " : " + Arrays.toString(p));
                    best[N] = f;
                    bestP[N] = p.clone();
                }
//                if (++i % 100000 == 0) {
//                    System.err.println(i + " / " + perms);
//                }


                /// 11 : 2.259259259259259 : [0, 1, 2, 4, 8, 3, 6, 10, 7, 5, 9]

            } while (nextPermutation(p));


            System.err.println("=============================================");
            for (int j = 1; j < MAXN; j++) {
                System.err.println(j + " : " + best[j] + " : " + Arrays.toString(bestP[j]));
            }
            System.err.println("=============================================");
        }
    }
}
