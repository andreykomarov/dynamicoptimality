import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Greedy {
    static int fst(long x) {
        return (int) (x >>> 32);
    }

    static int snd(long x) {
        return (int) (x << 32 >>> 32);
    }

    static long pack(int fst, int snd) {
        return (((long) fst) << 32) | snd;
    }

    static int process(List<Integer> queries) {
        ASS set = new ASS();
        TreeSet<Long> points = new TreeSet<>();
        for (int i = 0; i < queries.size(); i++) {
            int x = queries.get(i); // TODO slow for non-ArrayList
            set.add(x, i);
            points.add(pack(x, i));
            ArrayList<Long> newAdded = new ArrayList<>();
            boolean right = false;
            for (long point : points) {
                if (fst(point) > x)
                    right = true;
                if (!right)
                    continue;
                if (!set.isSatisfied(x, i, fst(point), snd(point))) {
                    newAdded.add(pack(fst(point), i));
                    x = fst(point);
                }
            }

            x = queries.get(i); // TODO
            boolean left = false;
            for (long point : points.descendingSet()) {
                if (fst(point) < x)
                    left = true;
                if (!left)
                    continue;
                if (!set.isSatisfied(x, i, fst(point), snd(point))) {
                    newAdded.add(pack(fst(point), i));
                    x = fst(point);
                }
            }

            for (long p : newAdded)
                set.add(fst(p), snd(p));
            points.addAll(newAdded);
        }

//        for (long p : points) {
//            System.err.print(fst(p) + " " + snd(p) + " // ");
//        }

        return points.size();
    }
}
